package com.shareobj.search;

import java.util.List;
import com.shareobj.search.R;
import com.shareobj.search.RowItem;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

public class CustomListViewAdapter extends ArrayAdapter<RowItem> {

    Context context;

    public CustomListViewAdapter(Context context, int resourceId,
                                 List<RowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtPhone;
        TextView txtTitle;
        TextView txtDesc;
        TextView txtDistance;
        TextView txtType;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            //holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.txtPhone = (TextView) convertView.findViewById(R.id.phone);
            holder.txtDistance = (TextView) convertView.findViewById(R.id.distance);
            //holder.txtType = (TextView) convertView.findViewById(R.id.type);
            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        //holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.txtPhone.setText(rowItem.getPhone());
        holder.txtDistance.setText(rowItem.getDistance());
        //holder.txtType.setText(rowItem.getType());
        holder.imageView.setImageResource(R.drawable.ic_launcher);
        Picasso.with(this.context).load(rowItem.getUrl()).into(holder.imageView);


        return convertView;
    }
}